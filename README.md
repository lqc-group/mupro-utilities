# No longer in use, go the mupro group's utilities repository, continued there
This repository contains the pre and post processing utilities for the MUPRO phase-field simulation package, as part of the MUPRO software suite.

# Contents
The utilities include the following parts:
### Pre-processing
1. High-throughput jobs creation script, inside the `script` folder.

### Post-processing
1. Ferroelectric domain structure visualization, inside the `plot-DomainFerro` folder.
2. Scalar variable volumetric visualization, inside the `plot-Scalar` folder, but for now it is still ready to be used yet.
3. Data backup tool, inside the `data-Backup` folder, which converts the default data output to HDF5 format collections.
4. Data restore tool, inside the `data-Restore` folder, restore the HDF5 format backup into the default output.

# Install and Compile
## Visualization
1. Download and install cmake3
2. Download the latest VTK from the official site.
3. Use command `ccmake3 .` to let cmake generate the make for compiling.
4. Set BUILD\_SHARED\_LIBS option to OFF, since we want to build a static library, for easier usage on other machines
5. Set VTK\_RENDERING\_BACKEND to OpenGL, since opengl2 is not supported on our server system.
6. Set CMAKE\_INSTALL\PREFIX to some directory you want the library to be installed to. Now there is a ready to use library in folder /gpfs/group/lqc3/default/VTK-static
7. Within the cmake interface, press c to configure and g to generate the makefile.
8. Exit cmake, `make -j8`, and then `make install`.
9. Build the library file in IO folder first.
10. Use cmake to build the individual program in each folder. Now the compiled executable is set to be dumped into a bin folder

## Backup


# Usage
## Visualization
## Backup
## High-throughput job preparation
