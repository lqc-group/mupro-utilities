## General batch job submission functions
**Script Name:** condition-batch.sh  
**Script Usage:** ```source condition-batch.sh```and then use individual function as normal bash commands. Should add the source command to .bashrc file for future easy access.  
**Script Description:**


Improved version of high throughput batch job submission functions based on the previous general-batch.sh (which is now in the deprecate folder). The major improvement over the previous version is that now you can specify a conditional statement that determines whether the current variables will be used to create a folder and modify the input files.
Now the workflow is also a bit different. First, an intermediate batchList.txt file is generated. And all of the following steps, such as folder creation, file copying, keywords replacement, folder-wise command execution is based on information in this batchList.txt file. 
The structure of the batchList.txt file is as follows:
1. The whole file is separated by "|" between columns
2. The first line is a header line. first column is the deliminator between keyword and value pairs in the created folder names, such as the "+" sign in "3+TEM_298+exx_0.1", default value is "+". You may use "/" for this so that rather than a single layer of folders, subfolders are generated. Last column is the original conditional statement. And all other columns in between are the keywords in input files to be replaced by some values.
3. The following lines are all substituting variables, each line will be used to generate one folder in the next stage. The first column is the index for current variable set. The last column is the conditional statement with keywords replaced by the corresponding variables. The middle columns are the sweeping parameters.

### Contained functions:
1. writeBatchList: Create the batchList.txt file for later usage. 
2. makeFolders: Create the folder structure, copy the necessary files into those folder, and replace the keywords in the files with provided variables. All based on information from batchList.txt file. 
3. execFolders: Execute command for folders that is specified by the batchList.txt file.
4. batchAllCommand: go into all subfolder rather than specified ones.

### Function Usage
##### writeBatchList [$1:keywords] [$2:replacement\_variable] [$3:conditional\_statement] [$4:deliminator] 
- $1: Use "#" as the separator between keywordes, default if the free foramt style of keyword, which means the whole line will be replace with the keyword and value pair. You may choose to use the fix format style by adding a "@" sign before the keyword, which means only the keyword itself will be replaced by the sweeping value,e.g. "SYSDIM#TEM", "TEM#@exx#@eyy"
- $2: Use "#" as the separator between different variable, and " " as separator for one variable but different values, then ":" as separator for different numbers 
 for one replacement (since it's replacing the whole line, there may be more than more numbers) e.g. "100:100:100 200:200:200#200 250 300" 
- $3: awk style conditional statement, you may use the keyword to represent the current variable value, e.g. 'exx>=eyy&&"REALDIM"=="SYSDIM"', which means only those cases that exx are greater or equal than eyy and SYSDIM are the same as REALDIM will be generated, notice the quotation mark around REALDIM and SYSDIM because the value for these two keywords are not one number, comparison between they should be comparison between strings.
- $4: The deliminator to be used for later folder generation stage for separating keyword, value pair. Default value is "+", you can set it to "/" which will generate subfolders structures, and each level is related to one variable changed, e.g. if you don't set this argument, then the folder will look like 01+TEM_350+exx_0.3, if you set it to be "/", then the folder will be like TEM_350/exx_0.3/


##### makeFolders [$1:file\_list] [$2:start\_index] [$3:end\_index]
- $1: Use " " as the separator between files,  add "&" before the file name you want to be processed as free format file, "@" before the file name you want to be processed as fix format file, and nothing for files just copy but not processed. e.g. "&input.in @ferro.pbs". Notice free format file can contains both free format type variables and fix type variables, which means the substitution of variables will be determined by the type you set for individual keyword, and fix format file
  can only have fix format style variables, which means no matter what style is set for the keywords, it will be treated as fix format style when replacement is made for fix format files.
- $2: Optional argument that set the starting row/index(inside the batchList.txt file) for folder generation,d efault is 1, so starting from index=1 of batchList.txt which is the second row.
- $3: Optional argument that set the ending row/index(inside the batchList.txt file) for folder generation,d efault is the line number of the batchList.txt file, so folder creation will end when it reach the last line of batchList.txt file.
##### execFolders [$1:command] [$2:start\_index] [$3:end\_index]
- $1: The command you want to execute for each generated folder from makeFolders, you can use the keyword here to represent the variable value within the folder, e.g. echo TEM,exx,eyy
- $2: Optional argument that set the starting row/index(inside the batchList.txt file) for folder generation,d efault is 1, so starting from index=1 of batchList.txt which is the second row.
- $3: Optional argument that set the ending row/index(inside the batchList.txt file) for folder generation,d efault is the line number of the batchList.txt file, so folder creation will end when it reach the last line of batchList.txt file.

##### batchAllCommand [$1:command\_string]  
- $1: A command in the form of string, that is the words you would type in terminal to execute the command, e.g. "qsub ferro.pbs"

### Example Usage
``` 
folders="TEM#@exx#@eyy#REALDIM#SYSDIM"   # separated by "#", for fix format style of keyword replacment insert @ in the front
files="&input.in pot.in @ferro.pbs"     # & for free format file, @ for fix format file, nothing for just copy
TEM="298 350 560"                       # each variable is separated by " ", use ":" when the variable contains more than one number
exx="0.1 0.2 0.3"
eyy="0.1 0.2 0.3"
realdim="123:234:234 456:567:678"
sysdim="123:234:234 456:567:678"
variables="${TEM}#${exx}#${eyy}#${realdim}#${sysdim}" # different variables are separated by #"
writeBatchList "${folders}" "${variables}" 'exx>=eyy&&"REALDIM"=="SYSDIM"'     # No 4th argument, default of + is used
makeFolders "${files}"
execFolders "echo TEM,exx,eyy,REALDIM"
```

