function getFirstString(){
	# This function separate the string into two parts
	# input: $2: separator, $1:string
	# return: first string before the first separator

	local separator=$2
	local parse_string=$1

    echo ${parse_string}|cut -d $separator -f1 | xargs
}

function getRemainString(){
	# This function separate the string into two parts
	# input: $2: separator, $1:string
	# return: the remaining string after the first separator

	local separator=$2
	local parse_string=$1


    local columns=$(echo $parse_string | awk -v sep=$separator 'BEGIN{FS=sep}{print NF;exit}')
    if [[ $columns > 1 ]]; then
        echo ${parse_string} | cut -d $separator -f2- | xargs
    else
        echo ""
    fi
}

function stdOutWithTab(){
	# redirect the command output through paste command that add
	# certain level of tab before
	# input:1 .tab levels 2. the tmp file location
	local level=$1
	local tab_string=""
    local i=0
	for (( i = 0; i < ${level}; i++ )); do
		tab_string=${tab_string}$'\t'
	done
	exec 3>&1
	exec 1> >(sed -e $"s/^/${tab_string}/g" >&3)
}

function restoreStdOut(){
	# restore the normal command output behaviour
	exec 1>&3 3>&-
}

function getColumnOfIndex(){
    # get the value at $1 row, $2 column in the batchList.txt
    local index=$1
    local index_1=$(($1+1))
    local line=$(sed "${index_1}q;d" batchList.txt)
    local column=$2
    local var=$(echo ${line} | awk -v num=$column 'BEGIN{FS="|"}{print $num}' | xargs )
    echo $var
}

function getVariableOfIndex(){
    # get the variables in row $1 of file batchList.txt, note the variableis are separated by "|"
    local index=$1
    local index_1=$(($1+1))
    local var=$(sed "${index_1}q;d" batchList.txt | cut -d "|" -f2- | rev | cut -d "|" -f2- | rev)
    echo $var
}

function getVariableNumber(){
    # get the amount of variables in batchList.txt, which equals to number of columns - 2,
    # since the first column is index, and the last column is condition
    local columns=$(awk 'BEGIN{FS="|"}{print NF-2;exit}' batchList.txt)
}
function increaseIndex(){
    # increase multidimension index,
    # $1 is the current index, $2 is the boundary of the index
    # e.g. $1=1 1 1, $2=2 2 2, and the result will be 1 1 2
    # e.g. $1=1 1 2, $2=2 2 2, and the result will be 1 2 1
    # when the largest value is given, then the output will be -1
    # e.g. $1=2 2 2, $2=2 2 2, and the result will be -1
    local current_index=($2)
    local index_bound=($1)
    local index_count=${#current_index[@]}
    local increased=false

    for (( i = index_count-1; i >= 0; i-- )); do
        if [[ ${index_bound[$i]} > ${current_index[$i]} ]]; then
            increased=true
            current_index[$i]=$((${current_index[$i]}+1))
            break
        else
            current_index[$i]=1
        fi
    done
    if [[ $increased == false ]]; then
        echo -1
    else
        echo ${current_index[@]}
    fi
}


function replaceKeywordsString(){
    # replacing the keywords in a string with the values provided
    # $1 is the string you want to process, $2 is the list of keywords separated by $4
    # $3 is the list of variables separated by $4
    local string=$1
    local keyword_list=$2
    local variable_list=$3
    local sep=$4
    local keyword=$(getFirstString "${keyword_list}" "$sep")
    local keyword_remain=$(getRemainString "${keyword_list}" "$sep")
    local variable=$(getFirstString "${variable_list}" "$sep")
    local variable_remain=$(getRemainString "${variable_list}" "$sep")
    if [[ ${keyword:0:1} == "@" ]]; then
        local out=$(echo "$string" | sed "s/${keyword:1}/${variable}/g")
    else
        local out=$(echo "$string" | sed "s/${keyword}/${variable}/g")
    fi
    if [[ ! -z ${keyword_remain} ]]; then
        out=$(replaceKeywordsString "$out" "${keyword_remain}" "${variable_remain}" $sep)
    fi

    echo $out
}

function replaceKeywordsStringIndex(){
    # replacing the keywords in a string with the values of specific line in batchList.txt file
    # $1 is the string you want to process, $2 specify the line number that contains the variable values
    # you want to inserted. The keywords are obtained from the first line of the batchList.txt file
    local string=$1
    local index=$2
    local var_name=$(getVariableOfIndex 0)
    local var_val=$(getVariableOfIndex $index)
    (replaceKeywordsString "$string" "${var_name}" "${var_val}" "|")
}

function replaceKeywordsFile(){
    # replacing the keywords in a file with the values provided
    # $1 the file name, $2 keywords, $3 the substituting value, $4 the separator for keywords and value
    # & sign before file name means its a free format file, @ sign before file name means its a fix format file
    # @ sign before variable name (keyword) means substituting this variable with the fix format style
    # free format file can have both free format style variable and fix format variable
    # fix format file can only have fix format variable
    local file=$1
    local var_name=$2
    local var_val=$3
    local sep=$4
    local columns=$(echo ${var_name} | awk 'BEGIN{FS="|"}{print NF;exit}')

    for (( i = 0; i < columns; i++ )); do
        local variable_name=$(getFirstString "${var_name}" $sep)
        var_name=$(getRemainString "${var_name}" $sep)
        local variable_val=$(getFirstString "${var_val}" $sep)
        var_val=$(getRemainString "${var_val}" $sep)
        if [[ ${file:0:1} == "&" || ${file:0:1} == "@" ]]; then
            if [[ ${file:0:1} == "&" && ${variable_name:0:1} != "@" ]]; then
                # free format
                sed -i "/${variable_name}/c ${variable_name} = ${variable_val}" ${file:1}
            else
                # fix format
                if [[ ${variable_name:0:1} == "@" ]]; then
                    sed -i "s/${variable_name:1}/${variable_val}/g" ${file:1}
                else
                    sed -i "s/${variable_name}/${variable_val}/g" ${file:1}
                fi
            fi
        fi
    done
}

function replaceKeywordsFileIndex(){
    # replace the keywords in file with value from a row in batchList.txt
    # $1 the file name, $2 the row index
    local file=$1
    local index=$2
    local var_name=$(getVariableOfIndex 0)
    local var_val=$(getVariableOfIndex $index)
    (replaceKeywordsFile "$file" "${var_name}" "${var_val}" "|")
}


function getFolderName(){
    # obtain the folder name based on variable name and value
    # $1 the index value, which gives you the variable value
    # $2 the separator between different variables, default is "+", you may use "/" to create subfolders
    local index=$1
    local sep=$(getColumnOfIndex 0 1)
    # local sep=${2:-"+"}
    local index_1=$(($1+1))
    local first_line=$(getVariableOfIndex 0)
    local line=$(getVariableOfIndex $index)
    local columns=$(awk 'BEGIN{FS="|"}{print NF;exit}' batchList.txt)
    local columns_1=$(($columns-1))
    local last_index=$(($(wc -l < batchList.txt)-1))
    local digits=${#last_index}
    local pad_index=$(printf "%0${digits}i" $index)
    if [[ ${sep} == "/" ]]; then
        local name=""
    else
        local name=${pad_index}${sep}
    fi
    local var_name="name"
    local var_val="value"

    for (( j = 2; j < columns; j++ )); do
        var_name=$(getColumnOfIndex 0 $j | xargs)
        if [[ ${var_name:0:1} == "@" ]]; then
            var_name=${var_name:1}
        fi
        var_val=($(getColumnOfIndex $index $j | xargs))
        var_val=$(IFS="_" ; echo "${var_val[*]}" ; IFS=" " | xargs)
        if [[ $j == $columns_1 ]]; then
            sep=""
        fi
        name=${name}${var_name}_${var_val}${sep}
    done
    echo $name
}

function createFolderConditionOneLine(){
    # create the one folder based on information of one row in the batchList.txt
    # and then copy the files into the folder
    # $1 the index of the row, $2 the list of files to be copied into the folder
    local index=$1
    local file_list=($2)
    # local sep=$(getColumnOfIndex 0 1)
    name=$(getFolderName ${index})
    echo "Create folder " ${name}
    mkdir -p $name
    for file in "${file_list[@]}"; do
        if [[ ${file:0:1} == "&" || ${file:0:1} == "@" ]]; then
            cp ${file:1} $name/${file:1}
            (replaceKeywordsFileIndex "${file:0:1}$name/${file:1}" $index )
        else
            cp $file $name/$file
            (replaceKeywordsFileIndex "$name/$file" $index )
        fi
    done
}

function printInfo(){
    #print the information when writing the batchList.txt file
    local name=$1
    local val=$2
    local sep=$3
    local first_name=$(getFirstString "$name" "$sep")
    local remain_name=$(getRemainString "$name" "$sep")
    local first_val=$(getFirstString "$val" "$sep")
    local remain_val=$(getRemainString "$val" "$sep")
    local first_sep=$(echo "$first_val" | sed 's/:/ /g')

    if [ ! -z "${remain_name}" ]; then
        echo -n ${first_name}=${first_val} ," "
        printInfo "${remain_name}" "${remain_val}" $sep
    else
        echo -n ${first_name}=${first_val}
        printf "\n"
    fi

}

function writeBatchOneline(){
    # Write one line into the batchList.txt file
    # $1 is all of the column values in the line
    local first=$(getFirstString "$1" "#")
    local remain=$(getRemainString "$1" "#")
    local first_sep=$(echo "$first" | sed 's/:/ /g')

    if [ ! -z "${remain}" ]; then
        printf '%15s' "${first_sep}" >> "batchList.txt"
        echo -n " | " >> "batchList.txt"
        writeBatchOneline "$remain"
    else
        printf '%15s' "${first}" >> "batchList.txt"
        printf "\n" >> "batchList.txt"
    fi
}



function makeFolders(){
    # create all folders and copy the files into the folders
    # $1 the file list that need to be copied, $2 starting index, $3 ending index of batchList.txt file
    local last_index=$(($(wc -l < batchList.txt)-1))
    local start=${2:-"1"}
    local end=${3:-"${last_index}"}
    local file_list=$1
    local i=0
    # echo $start,"end",$end

    for (( i = start; i <= end; i++ )); do
        # echo $i
        (createFolderConditionOneLine ${i} "${file_list}")
    done
}

function execFolders(){
    # execute commands inside the folders, created from batchList.txt file
    # $1 the command to exec, notice you can use the keywords in the header line of batchList.txt file,
    # and it will be substituted by the variable values for each folder
    # $2, the starting index, $3 the ending index
    local last_index=$(($(wc -l < batchList.txt)-1))
    local start=${2:-"1"}
    local end=${3:-"${last_index}"}
    local command_string=$1
    local temp_cmd="temp command"
    local i=0
    local pwd=$(pwd)
    # local sep=$(getColumnOfIndex 0 1)

    for (( i = start; i <= end; i++ )); do
        local name=$(getFolderName ${i})
        temp_cmd=$(replaceKeywordsStringIndex "${command_string}" ${i})
        echo "Exec command: "${command_string} "in folder" ${name}
        stdOutWithTab 1
        cd $name
        (eval "${temp_cmd}")
        cd ${pwd}
        restoreStdOut
    done
}


function writeBatchList(){
    # create the batchList.txt file, which is a file used in later stage for creating folders or executing commands
    # $1 the keywords name separated by #, $2 the variable values separated by #
    # $3 the condition string , $4 the separator for creating folders
    # the structure of batchList.txt file is like a table, each column is separated by "|"
    # the first line of batchList.txt is the header line, first value is the separator for
    # different variables when creating folder use the makeFolders command
    # the last column is the condition column, and all columns in between are the variable columns
	local folder_string=$1
	local variable_string=$2
    local condition_string=$3
    local sep=${4:-"+"}
    local variable_index=$(echo $variable_string | awk 'BEGIN{RS="#"}{print 1}')
    local variable_list_count=$(echo $variable_string | awk 'BEGIN{RS="#"}{print NF}')
    local index=0

    local current_condition=${condition_string}

    if [[ -f batchList.txt ]]; then
        rm batchList.txt
    fi

    (writeBatchOneline "${sep}#${folder_string}#$condition_string")

    while [[ ${variable_index} != -1 ]]; do
        current_variable=$(echo $variable_string | awk -v var="${variable_index}" 'BEGIN{RS="#";split(var,list," ")}{if(NR>1){print "#"$list[NR];}else{print $list[NR];}}')
        current_condition=$(replaceKeywordsString "${condition_string}" "${folder_string}" "${current_variable}" "#" )
        # if $(eval [[ ${current_condition} ]]);then
        if $(awk 'BEGIN{if('${current_condition}'){print "true"}else{print "false"}}');then
            index=$(($index+1))
            echo "Condition fulfilled." ${current_condition}
            stdOutWithTab 1
            echo -ne $index "\t"
            (printInfo "$folder_string" "$current_variable" "#")
            (writeBatchOneline "${index}#${current_variable}#${current_condition}")
            restoreStdOut
        else
            echo "Condition not fulfiled. " ${current_condition}
        fi

        variable_index=$(increaseIndex "${variable_list_count[@]}" "${variable_index[@]}")
    done

}

function batchAllCommand(){
	# This command will loop through all folders and execute a command
	# input: 1. the command you want to execute
	# 2. is optional integer that tune the indentation before output

	local command_string="$1"
	local batch_depth=0
	local batch_depth_plus=1
	local folder_array=($(find . -maxdepth 1 -type d))
	local current_dir=$(eval pwd)
	local dir=""
	if [ ! -z "$2" ]; then
		let batch_depth=$2
	fi
	stdOutWithTab ${batch_depth}
	if [ ${#folder_array[@]} = 1 ]; then
		echo "Executing ${command_string}"
		eval "${command_string}"
	fi
	for dir in */ ; do
		if [[ -d ${dir} ]]; then
			echo "Going into ${dir}, from ${current_dir}"
			cd  ${dir}
			let batch_depth_plus=(${batch_depth}+1)
			restoreStdOut
			batchAllCommand "${command_string}" ${batch_depth_plus}
			stdOutWithTab ${batch_depth}
			cd ..
			echo "Return from ${dir}, to ${current_dir}"
		fi
	done

	restoreStdOut
	sleep 0.1
}
