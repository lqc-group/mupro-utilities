#! /bin/bash
#FerroLoop for Stability Diagrams

#Written by Jacob Zorn
#11/1/2017
#Batch script that is useful in creating a png files of domains using FerroDomainVTK and a visualPolar.in file.
#You will need to adjust the parameters of your visualPolar.in file prior to running this script. Additionally, you need to copy the visualPolar.in file into the parent directory of your research jobs.
#Example: Parent Directory = PbTiO3_Stability; Sub-Directories = 323,373,423, etc.; Copy visualPolar.in into the PbTiO3_Stability directory and then the program will copy visualPolar.in to whereever it needs to go.

#Create a vector that incorporates all the different sub-directory names for your research
for temperature in 323 373 423 473 523 573

#Loop through all the various sub-directories
do
	let temp=$temperature
	cd $temperature
	cp ../visualPolar.in .
	#Create a vector for the sub-directories located in a "parent" sub-directory
	for strain in N09 N05 N04 N03 N02 N01 P00 P01 P02 P03 P04 P05 P09
	#Loop through all the various sub-directories
	do
		cd $strain
		cp ../visualPolar.in .
		#Print the "parent" sub-directory and the current sub-directory; Later additions includes adding the word "Started" so for example the print out reads "Started 323 N09"
		echo $temperature $strain
		#Run FerroDomainVTK according to parameters in visualPolar.in
		FerroDomainVTK
		#Print the "parent" sub-directory and the current sub-directory; Later additions includes adding the word "Finished" so for example the print out reads "Finished 323 N09"
		echo $temperature $strain
		cd ..
	done
	cd ..
done
