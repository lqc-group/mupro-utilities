#! /bin/bash
#Batch Input Files, Potential Files, PBS Files, Directory Creation, and Job Submission
#Written by Jacob Zorn
#10/30/2017
#Script I wrote for batch creation of directories and batch submission of jobs to the Open Queue, easily transferable for sending to our group queue if necessary.

#Create the Directory Vector, I use these to name folders
vector=( 273 323 373 423 473 523 573 )
#Create vector with your strain names, I use these to name folders inside my temperature folder.
strain=(N09 N05  N04 N03 N02 N01 P00 P01 P02 P03 P04 P05 P09 )
#Create a vector with your strain magnitudes, necessary for fixing the strain magnitudes in each individual input file
strainmag=(-0.009 -0.005 -0.004 -0.003 -0.002 -0.001 0.000 0.001 0.002 0.003 0.004 0.005 0.009 )
#Vector that possesses the names for each of my jobs, i.e. Open Circuit Negative Strain 0.001 = OpenN01
ferro=( OpenN09 OpenN05 OpenN04 OpenN03 OpenN02 OpenN01 OpenP00 OpenP01 OpenP02 OpenP03 OpenP04 OpenP05 OpenP09 )


#Inital Loop for each temperature
for temperature in "${vector[@]}"
do
	let temp=$temperature
	#Making a temperature directory/folder
	mkdir $temperature
	cd $temperature
	cp ../input.in .
	cp ../ferro.pbs .
	cp ../pot.in .
	#Replacing the string "mytemp" with the temperature of these particular jobs
	sed -i "s/mytemp/${temp}/g" input.in
	let I=0	
	for eachstrain in "${strain[@]}"
	do
		thisstrain=”${strainmag[$I]}”
		thisferro=${ferro[$I]}
		mkdir $eachstrain
		cd $eachstrain
		cp ../input.in .
		cp ../ferro.pbs .
		cp ../pot.in .
		let lxstrain=$eachstrain
		#Replacing a string "mystrain" with the value of strain I defined previously
		sed -i "s/mystrain/${thisstrain}/g" input.in
		#Since the strain magnitude is seen as a string I remove the "" that are around it
		sed -i "s/[“”]//g" input.in
		#Replacing the string "FerroName" with the name of this particular job in the ferro.pbs script
		sed -i "s/FerroName/${thisferro}/g" ferro.pbs
		#If you wish to submit every job at once you can simply uncomment the line below and it will submit the jobs for you. Otherwise there is another script that will do that for you.
		#qsub ferro.pbs
		cd ..
		I=$I+1
	done
	
	cd ..
done
