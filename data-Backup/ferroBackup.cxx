#include <DataHDF5IO.h>
#include <TxtHDF5IO.h>
#include <FreeFormatOneLine.h>
#include <FreeFormatParser.h>
#include <TableHDF5IO.h>
#include <H5File.h>
#include <ScreenPrint.h>
#include <string.h>
#include <unistd.h>
#include <FileExist.hpp>
/* using namespace H5; */

int main(int argc, char *argv[]){
    DataHDF5IO polarData;
    TableHDF5IO inputData,energyout,potData,fixData;
    TxtHDF5IO txtData;
    H5File comment;
    FreeFormatParser structRead;
    ScreenPrint print;
    print.setWidth(120);
    print.setColumnWidth(60);
    print.setNumberWidth(11);
    std::string h5Name,nameHold,strHold;
    bool controlFileExist=false;
    vector<fileGroup> datFile,freeFile,fixFile,jsonFile,tdFile,txtFile,message;
    fileGroup fileGroup_hold;
    vector<string> vecHold,groupName;
    int i,start;

    structRead.setFilename("backupRestore.in");
    controlFileExist = structRead.parse();

    h5Name = "backup.h5";
    if (controlFileExist) {
        print.printCenter("Start to read backupRestore.in file",'-');
        vecHold.clear();
        if (structRead.firstKeyExist("FILE")) {
            vecHold = structRead.getFirstLevel("FILE");
            h5Name = vecHold[0];
        }
        groupName = findObjects(structRead,"GROUP");
        freeFile = findObjects(structRead,"FREE",groupName);
        message = findObjects(structRead,"COMMENT",groupName);
        fixFile = findObjects(structRead,"FIX",groupName);
        tdFile = findObjects(structRead,"TIME",groupName);
        jsonFile = findObjects(structRead,"JSON",groupName);
        txtFile = findObjects(structRead,"TXT",groupName);
//        if (structRead.firstKeyExist("FREEINPUT")) {
//            vecHold = structRead.getFirstLevel("FREEINPUT");
//            for (auto &it : vecHold) {
//                istringstream iss(it);
//                while(iss){
//                    iss >> nameHold;
//                    freeFileName.push_back(nameHold);
//                }
//            }
//        }
//        if (structRead.firstKeyExist("COMMENT")) {
//            message = structRead.getFirstLevel("COMMENT");
//        }
//        if (structRead.firstKeyExist("FIXINPUT")) {
//            vecHold = structRead.getFirstLevel("FIXINPUT");
//            for (auto &it : vecHold) {
//                istringstream iss(it);
//                while(iss){
//                    iss >> nameHold;
//                    fixFileName.push_back(nameHold);
//                }
//            }
//        }
//        if (structRead.firstKeyExist("TIMEDEPEND")) {
//            vecHold = structRead.getFirstLevel("TIMEDEPEND");
//            for (auto &it : vecHold) {
//                istringstream iss(it);
//                while(iss){
//                    iss >> nameHold;
//                    tdFileName.push_back(nameHold);
//                }
//            }
//        }
//        if (structRead.firstKeyExist("JSON")) {
//            vecHold = structRead.getFirstLevel("JSON");
//            for (auto &it : vecHold) {
//                istringstream iss(it);
//                while(iss){
//                    iss >> nameHold;
//                    jsonFileName.push_back(nameHold);
//                }
//            }
//        }
//        if (structRead.firstKeyExist("TXT")) {
//            vecHold = structRead.getFirstLevel("TXT");
//            for (auto &it : vecHold) {
//                istringstream iss(it);
//                while(iss){
//                    iss >> nameHold;
//                    txtFileName.push_back(nameHold);
//                }
//            }
//        }



    }else {

        if(argc > 1){
            fileGroup_hold.name = "energy_out.dat";
            fileGroup_hold.group = "Output";
            tdFile.push_back(fileGroup_hold);
            print.printCenter("Using the default energy_out.dat",' ');
            print.printCenter("Get dat file name from command line",'-');
            strHold = argv[1];
            if (strHold.substr(strHold.find_last_of(".")+1) == "h5") {
                nameHold = "Output to the " + strHold;
                print.printCenter(nameHold,' ');
                h5Name = strHold;
                start = 2;
            }else{
                print.printCenter( "Output to the default backup.h5.",' ');
                h5Name = "backup.h5";
                start = 1;
            }
            for ( i = start; i < argc; i++) {
                strHold = argv[i];
                if (strHold == "-m") {
                    i++ ; 
                    /* nameHold = argv[i]; */
                    fileGroup_hold.name = argv[i];
                    fileGroup_hold.group = "/";
                    message.push_back(fileGroup_hold);
                }else{
                    nameHold = "Using the data file " + strHold;
                    fileGroup_hold.name = strHold;
                    fileGroup_hold.group = "Output";
                    datFile.push_back(fileGroup_hold); 
                    print.printCenter(nameHold,' ');
                }
            }

        }else{
            print.printLeft("Warning, no backupRestore.in file, and no command line argument ",' ');
            print.printCenter("Output to the default backup.h5, only input is backuped.",' ');
            h5Name = "backup.h5";
        }
        if (fileExist("input.in")) {
            fileGroup_hold.name = "input.in";
            fileGroup_hold.group = "Input";
            freeFile.push_back(fileGroup_hold);
            print.printCenter("Using the default input.in",' '); 
        }else if(fileExist("inputN.in")){
            fileGroup_hold.name = "inputN.in";
            fileGroup_hold.group = "Input";
            fixFile.push_back(fileGroup_hold);
            print.printCenter("Using the default inputN.in",' '); 
        }else if(fileExist("parameter.in")){
            fileGroup_hold.name = "parameter.in";
            fileGroup_hold.group = "Input";
            fixFile.push_back(fileGroup_hold);
            print.printCenter("Using the default parameter.in",' '); 
        }

        if (fileExist("structgen.in")) {
            fileGroup_hold.name = "structgen.in";
            fileGroup_hold.group = "Input";
            freeFile.push_back(fileGroup_hold);
            print.printCenter("Using the default structgen.in",' ');
        }

        if (fileExist("Polar.in")) {
            fileGroup_hold.name = "Polar.in";
            fileGroup_hold.group = "Input";
            datFile.push_back(fileGroup_hold);
            print.printCenter("Using the default Polar.in",' ');
        }


        if (fileExist("pot.in")) {
            fileGroup_hold.name = "pot.in";
            fileGroup_hold.group = "Input";
            jsonFile.push_back(fileGroup_hold);
            print.printCenter("Using the default pot.in",' ');
            int i = 1;
            nameHold="pot"+to_string(i)+".in";
            while(fileExist(nameHold.c_str())){
                i = i+1;
                strHold="pot"+to_string(i)+".in";
                fileGroup_hold.name = strHold;
                fileGroup_hold.group = "Input";
                jsonFile.push_back(fileGroup_hold);
                nameHold = "Using the default " + strHold;
                print.printCenter( nameHold,' ');
            }
        }

        if (fileExist("batch.sh")) {
            fileGroup_hold.name = "batch.sh";
            fileGroup_hold.group = "Input";
            txtFile.push_back(fileGroup_hold);
            print.printCenter("Using the default batch.sh",' ');
        }
        if (fileExist("Ferroelectric.pbs")) {
            fileGroup_hold.name = "Ferroelectric.pbs";
            fileGroup_hold.group = "Input";
            txtFile.push_back(fileGroup_hold);
            print.printCenter("Using the default Ferroelectric.pbs",' ');
        }
        if (fileExist("backupRestore.in")) {
            fileGroup_hold.name = "backupRestore.in";
            fileGroup_hold.group = "Input";
            txtFile.push_back(fileGroup_hold);
            print.printCenter("Using the default backupRestore.in",' ');
        }

    }

    if (message.size() > 0) {
        comment.setH5FileName(h5Name);
        comment.addRootComment("Comment",message[0].name);
    }


    for (auto &it : datFile) {
        polarData.setDatFileName(it.name);
        polarData.setH5FileName(h5Name);
        polarData.readDatwriteH5(it.group);
        /* polarData.outputH5("output"); */
        polarData.cleanDat();
    }
    std::cout << "going to the time dependent" << std::endl;
    for (auto &it : tdFile) {
        energyout.setDatFileName(it.name);
        energyout.setH5FileName(h5Name);
        energyout.readTimeDependentFile();
        energyout.outputH5(it.group);
        energyout.cleanDat();
    }

    std::cout << "going to the input data" << std::endl;
    for (auto &it : freeFile) {
        inputData.setDatFileName(it.name);
        inputData.setH5FileName(h5Name);
        inputData.readFreeFormatFile();
        inputData.outputH5(it.group);
        inputData.cleanDat();
    }

    std::cout << "going to the potential data" << std::endl;
    for (auto &it : jsonFile) {
        potData.setDatFileName(it.name);
        potData.setH5FileName(h5Name);
        potData.readJSONFile();
        potData.outputH5(it.group);
        potData.cleanDat();
    }
    std::cout << "going to the fix data" << std::endl;
    for (auto &it : fixFile) {
        fixData.setDatFileName(it.name);
        fixData.setH5FileName(h5Name);
        fixData.readFixFormatFile();
        fixData.outputH5(it.group);
    }
    std::cout << "going to the txt data" << std::endl;
    for (auto &it : txtFile) {
        txtData.setDatFileName(it.name);
        txtData.setH5FileName(h5Name);
        txtData.readTXTFile();
        txtData.outputH5(it.group);
    }





    return 0;
}
